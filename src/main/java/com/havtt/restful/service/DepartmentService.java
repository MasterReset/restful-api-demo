/**
 * 
 */
package com.havtt.restful.service;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.havtt.restful.dao.DepartmentDao;
import com.havtt.restful.db.Department;

/**
 * @author havtt
 *
 */
@Path("/departments")
public class DepartmentService {
	// URL: http://localhost:8080/Restful/rest/departments
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Department> getDepartments_JSON() {
		List<Department> departments = DepartmentDao.getAllDepartments();
		return departments;
	}

	// URL: http://localhost:8080/Restful/rest/departments/{deptNo}
	@GET
	@Path("/{deptNo}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Department getDepartment(@PathParam("deptNo") String deptNo) {
		return DepartmentDao.getDepartment(deptNo);
	}

	// URL: http://localhost:8080/Restful/rest/departments
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Department addDepartment(Department dept) {
		return DepartmentDao.addDepartment(dept);
	}

	// URL: http://localhost:8080/Restful/rest/departments
	@PUT
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Department updateDepartment(Department dept) {
		return DepartmentDao.updateDepartment(dept);
	}

	@DELETE
	@Path("/{deptNo}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void deleteDepartment(@PathParam("deptNo") String deptNo) {
		DepartmentDao.deleteDepartment(deptNo);
	}
}

/**
 * 
 */
package com.havtt.restful.db;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author havtt
 *
 */

@XmlRootElement(name = "department")
@XmlAccessorType(XmlAccessType.FIELD)
public class Department {

	private String deptNo;
	private String deptAbbr;
	private String deptName;
	private String tel;
	private String fax;

	public Department() {

	}

	public Department(String deptNo, String deptAbbr, String deptName, String tel, String fax) {
		super();
		this.deptNo = deptNo;
		this.deptAbbr = deptAbbr;
		this.deptName = deptName;
		this.tel = tel;
		this.fax = fax;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getDeptAbbr() {
		return deptAbbr;
	}

	public void setDeptAbbr(String deptAbbr) {
		this.deptAbbr = deptAbbr;
	}

}

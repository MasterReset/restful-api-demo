/**
 * 
 */
package com.havtt.restful.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.havtt.restful.db.Department;

/**
 * @author havtt
 *
 */
public class DepartmentDao {
	private static final Map<String, Department> deptMap = new HashMap<String, Department>();

	static {
		initDepartments();
	}

	private static void initDepartments() {
		Department dept1 = new Department("0100000", "HR", "Human Resources", "010101", "111-11-111");
		Department dept2 = new Department("0200000", "IT", "Information Technology", "020202", "222-22-222");
		Department dept3 = new Department("0300000", "MA", "Marketing", "030303", "333-33-333");

		deptMap.put(dept1.getDeptNo(), dept1);
		deptMap.put(dept2.getDeptNo(), dept2);
		deptMap.put(dept3.getDeptNo(), dept3);
	}

	public static Department getDepartment(String deptNo) {
		return deptMap.get(deptNo);
	}

	public static Department addDepartment(Department dept) {
		deptMap.put(dept.getDeptNo(), dept);
		return dept;
	}

	public static Department updateDepartment(Department dept) {
		deptMap.put(dept.getDeptNo(), dept);
		return dept;
	}

	public static void deleteDepartment(String deptNo) {
		deptMap.remove(deptNo);
	}

	public static List<Department> getAllDepartments() {
		Collection<Department> depts = deptMap.values();
		List<Department> list = new ArrayList<Department>();
		list.addAll(depts);
		return list;
	}

	List<Department> list;
}
